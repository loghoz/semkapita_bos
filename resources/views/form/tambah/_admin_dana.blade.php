<div class="col-sm-6">
  <div class="form-group">
    <label for="tanggal" class="control-label col-sm-4">Tanggal</label>
    <div class="col-sm-8">
      <input type="date" name="tanggal" class="form-control" id="tanggal" placeholder="Dana Masuk">
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('tanggal') }}</small>
    </div>
  </div>
  <div class="form-group">
    <label for="dana" class="control-label col-sm-4">Dana Masuk</label>
    <div class="col-sm-8">
      <input type="text" name="dana" class="form-control" id="text" placeholder="Dana Masuk">
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('dana') }}</small>
    </div>
  </div>
  <div class="form-group">
    <label for="ket" class="control-label col-sm-4">Keterangan</label>
    <div class="col-sm-8">
      <input type="text" name="ket" class="form-control" id="ket" placeholder="Keterangan">
    </div>
    <div class="col-sm-4"></div>
    <div class="col-sm-8">
      <small class="text-danger">{{ $errors->first('ket') }}</small>
    </div>
  </div>

  <div class="btn-group pull-right">
    <button type="reset" class="btn btn-default">Batal</button>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </div>
</div>