@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Dana Masuk
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Dana Masuk</h3>
        </div>
        <div class="box-body">
          <form action="{{ route('dana.store') }}" method="POST" class="form-horizontal">
            @csrf
            @include('form.tambah._admin_dana')
          </form>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Dana</th>
                <th>Keterangan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($dana as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->tanggal->format('d/m/Y') }}</td>
                <td>{{ $item->dana }}</td>
                <td>{{ $item->ket }}</td>
                <td>
                  <form action="{{ route('dana.destroy',$item->id) }}" method="POST">

                    <a class="btn btn-primary" href="{{ route('dana.edit',$item->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection