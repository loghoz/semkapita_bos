<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dana extends Model
{
    protected $dates = ['tanggal'];
    protected $fillable = [
        'id_tahun', 'id_tahap', 'tanggal', 'dana', 'ket'
    ];
}
