<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'namabarang', 'satuan', 'harga'
    ];
}
