<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'saldo', 'ket'
    ];
}
