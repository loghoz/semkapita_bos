<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBarang extends Model
{
    protected $fillable = [
        'id_nota', 'id_barang', 'qty'
    ];
}
