<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKomponen extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'id_komponen', 'subkomponen'
    ];
}
