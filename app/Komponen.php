<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komponen extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'komponen'
    ];
}
