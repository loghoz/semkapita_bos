<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuKasUmum extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'id_nota', 'pajak', 'pph', 'uraian'
    ];
}
