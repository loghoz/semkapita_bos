<?php

namespace App\Http\Controllers\Web\Admin;

use App\Dana;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meta;
use App\Tahap;
use App\Tahun;

class DanaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $mtahun = Meta::where('id', 1)->get()->first();
        $tahun = Tahun::where('tahun', $mtahun->meta_value)->get()->first();
        $mtahap = Meta::where('id', 2)->get()->first();
        $tahap = Tahap::where('tahap', $mtahap->meta_value)->where('id_tahun', $tahun->id)->get()->first();

        $dana = Dana::where('id_tahun', $tahun->id)
            ->where('id_tahap', $tahap->id)->get()->all();

        return view('admin.dana.view', compact('no', 'dana'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $mtahun = Meta::where('id', 1)->get()->first();
        $tahun = Tahun::where('tahun', $mtahun->meta_value)->get()->first();
        $mtahap = Meta::where('id', 2)->get()->first();
        $tahap = Tahap::where('tahap', $mtahap->meta_value)->where('id_tahun', $tahun->id)->get()->first();

        $request->validate([
            'dana' => 'required',
            'ket' => 'required',
        ]);

        $data = $request->all();
        $data['id_tahun'] = $tahun->id;
        $data['id_tahap'] = $tahap->id;

        Dana::create($data);

        $notification = array(
            'message' => 'Dana masuk baru berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('dana.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
