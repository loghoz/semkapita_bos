<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable = [
        'id_tahun', 'id_tahap', 'id_subkomponen', 'koderek', 'tanggal', 'kpajak', 'kpph'
    ];
}
