<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaldosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_tahun')->unsigned();
            $table->foreign('id_tahun')->references('id')->on('tahuns')->onDelete('cascade');
            $table->bigInteger('id_tahap')->unsigned();
            $table->foreign('id_tahap')->references('id')->on('tahaps')->onDelete('cascade');
            $table->string('saldo');
            $table->string('ket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldos');
    }
}
