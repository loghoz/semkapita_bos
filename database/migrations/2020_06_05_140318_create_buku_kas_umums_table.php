<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuKasUmumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_kas_umums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_tahun')->unsigned();
            $table->foreign('id_tahun')->references('id')->on('tahuns')->onDelete('cascade');
            $table->bigInteger('id_tahap')->unsigned();
            $table->foreign('id_tahap')->references('id')->on('tahaps')->onDelete('cascade');
            $table->bigInteger('id_nota')->unsigned();
            $table->foreign('id_nota')->references('id')->on('notas')->onDelete('cascade');
            $table->string('pajak');
            $table->string('pph');
            $table->string('uraian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_kas_umums');
    }
}
