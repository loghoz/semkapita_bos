<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubKomponensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_komponens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_tahun')->unsigned();
            $table->foreign('id_tahun')->references('id')->on('tahuns')->onDelete('cascade');
            $table->bigInteger('id_tahap')->unsigned();
            $table->foreign('id_tahap')->references('id')->on('tahaps')->onDelete('cascade');
            $table->bigInteger('id_komponen')->unsigned();
            $table->foreign('id_komponen')->references('id')->on('komponens')->onDelete('cascade');
            $table->string('subkomponen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_komponens');
    }
}
