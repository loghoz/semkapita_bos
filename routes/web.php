<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/dana', 'Web\Admin\DanaController');
Route::resource('/admin/saldo', 'Web\Admin\SaldoController');
